module.exports ={ findProductbySupplier: function(products,suppliers,supplierId){
    let listprod
    for(let sup of suppliers){
        if(sup.id === Number(supplierId)){
            listprod= sup.products;
            break;
        }
    }
    
    products=products.filter( product => {
        for(let prod of listprod){
            if (product.name === prod.name) {
                return true;
            }
            return false;
        }
    });

    return products;
     
}}
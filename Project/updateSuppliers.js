module.exports ={ updateSuppliers: function(suppliers,id,newSupplier){
    for (let i = 0; i < suppliers.length; i++) {
        let supplier = suppliers[i]
        if (supplier.id === Number(id)) {
            suppliers[i] = newSupplier;
        }
    }
    return suppliers;
}}